package com.adamantdreamer.foundation.core;



@Path("database")
public class DatabaseConfig {
	
	public String type;

	public String folder;

	public String host;
	public String port;
	public String user;
	public String pass;
	
	public String primarySchema;
	
}
