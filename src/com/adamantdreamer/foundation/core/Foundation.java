package com.adamantdreamer.foundation.core;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.adamantdreamer.foundation.core.command.Ormcheck;
import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.WorldID;
import com.adamantdreamer.foundation.core.db.WorldIDS;
import com.adamantdreamer.foundation.core.listener.FoundationListener;
import com.adamantdreamer.quorm.core.Link;
import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.exception.ColumnMismatchException;
import com.adamantdreamer.quorm.link.mysql.MysqlLink;

public class Foundation extends FoundationPlugin {

	private static FoundationPlugin plugin; 
	private static DAO dao = null;
	
	static public FoundationPlugin getPlugin() {
		return plugin;
	}

	static public DAO dao() {
		return dao;
	}

	@Override
	public void onEnable() {
		plugin = this;
		
		DatabaseConfig config = null;
		
		// Load Configurations
		try {
			saveDefaultConfig();
			getConfig().options().copyDefaults(true);
			getConfig().getString("type");
			config = loadConfig(new DatabaseConfig());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		// Register Events / Commands
		register(new FoundationListener());
		
		register(new Ormcheck(), "ormcheck");
		
		
		
		// Load Database
		Link primaryLink = null;
		switch (config.type.toLowerCase()) {
		case "mysql":
			if (dao == null) {
				primaryLink = new MysqlLink(config.host, config.port, config.user, config.pass);
			} else if (dao.getLink() instanceof MysqlLink) {
				primaryLink = dao.getLink();
				((MysqlLink)primaryLink).setLogin(config.host, config.port, config.user, config.pass);
			} else {
				logError("Can not change SQL server type on reload.  Please restart Bukkit to enable.");
				dao = null;
				return;
			}
			
			try {
				primaryLink.connect();
				log("Connected to MySQL Server");
			} catch (Exception ex) {
				logError("Error connecting to MySQL Server", ex);
				return;
			}
			break;
		case "sqlite":
			//SQ Lite features are not enabled in Quorm yet.
			logError("SQLite features have not been implmented yet.");
			return;
		default:
			logError("Unrecongized connection type \"" + getConfig().getString("type") + "\", no database link enabled.");
			return;
		}

		dao = primaryLink.getDAO();
		try {
			Schema schema = new Schema(config.primarySchema, PlayerID.class, WorldID.class);
			dao.build(schema);
		} catch (SQLException | ColumnMismatchException e) {
			logError("Unable to build tables", e);
		}
		
		log("Enabled");
		
		//Load worlds
		for (World world: Bukkit.getServer().getWorlds()) {
			try {
				WorldIDS.get(world);
			} catch (SQLException e) {
				logError("Error syncing world data into database:", e);
			}
		}
	}

	@Override
	public void onDisable() {
		dao.getLink().close();
	}
	
	
	/**
	 * Retrieves the primary connection link for QuORM, or null if the link is not enabled.
	 */
	
}
