package com.adamantdreamer.foundation.core;

import java.lang.reflect.Field;
import java.util.logging.Level;

import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class FoundationPlugin extends JavaPlugin {

//// Logger ////
	
	public void log(String... text) {
		getLogger().info(text[0]);
		for (int i = 1; i < text.length; i++) {
			getLogger().info("\t" + text[i]);
		}
	}
	
	public void logError(String message) {
		getLogger().log(Level.SEVERE, "~~~ Error ~~~ " + message);
	}
	
	public void logError(String message, Exception ex) {
		getLogger().log(Level.SEVERE, "~~~ Exception ~~~ " + message + "\n{0}", ex);
	}	
	
//// Register ////
	
	public void register(CommandExecutor executor, String... commands) {
		for (String command : commands) {
			getCommand(command).setExecutor(executor);
		}
	}
	
	public void register(Listener... listeners) {
		for (Listener listener: listeners) {
			getServer().getPluginManager().registerEvents(listener, this);
		}
	}
	
//// Configuration File ORM ////
	
	public void loadConfig(Object... intoObjects) throws IllegalArgumentException, IllegalAccessException {
		for (Object intoObject: intoObjects) {
			loadConfig(intoObject);
		}
	}

	public <T> T loadConfig(T intoObject) throws IllegalArgumentException, IllegalAccessException {
		String basePath = getPath(intoObject.getClass());
		for (Field field: intoObject.getClass().getDeclaredFields()) {
			String path = basePath + getPath(field);
			if (getConfig().contains(path)) {
				field.setAccessible(true);
				switch (field.getType().getSimpleName().toLowerCase()) {
				case "string":
					field.set(intoObject, getConfig().getString(path));
					break;
				case "int":
				case "integer":
					field.set(intoObject, getConfig().getInt(path));
					break;
				}
				
			}
		}
		return intoObject;
	}	
	
	public void saveConfig(Object... fromObjects) throws IllegalArgumentException, IllegalAccessException {
		for (Object fromObject: fromObjects) {
			saveConfig(fromObject);
		}
	}
	
	public <T> T saveConfig(T fromObject) throws IllegalArgumentException, IllegalAccessException {
		String basePath = getPath(fromObject.getClass());
		for (Field field: fromObject.getClass().getDeclaredFields()) {
			String path = basePath + getPath(field);
			field.setAccessible(true);
			getConfig().set(path, field.get(fromObject));
		}
		return fromObject;
	}	

	private String getPath(Class<?> clazz) {
		return (clazz.isAnnotationPresent(Path.class)
				? clazz.getAnnotation(Path.class).value() + "."
				: "");
	}

	private String getPath(Field field) {
		return (field.isAnnotationPresent(Path.class) 
				? field.getAnnotation(Path.class).value()
				: field.getName());
	}	
	
	
}
