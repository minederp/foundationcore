package com.adamantdreamer.foundation.core.command;

import java.sql.SQLException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.adamantdreamer.foundation.core.Foundation;

public class Ormcheck implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (!sender.isOp()) {
			sender.sendMessage("You don't have permission to use ormcheck.");
			return true;
		}
		
		sender.sendMessage("Foundation Core ORM Status:");
		try {
			sender.sendMessage("  Connection: " 
					+ (!Foundation.dao().getLink().getConnection().isClosed()));
		} catch (SQLException e) {
			sender.sendMessage("  Connection: Error");
		}
		
		return true;
	}

}
