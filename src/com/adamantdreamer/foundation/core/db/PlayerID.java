package com.adamantdreamer.foundation.core.db;

import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.adamantdreamer.foundation.core.Foundation;
import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.Default;
import com.adamantdreamer.quorm.define.DefineIndex;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Ignore;
import com.adamantdreamer.quorm.define.Index;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Var;
import com.adamantdreamer.quorm.index.IndexType;
import com.adamantdreamer.quorm.query.Query;

@SuppressWarnings("unused")
@DefineIndex(value="uuidIndex", type=IndexType.UNIQUE_KEY)
public class PlayerID {
	@Ignore
	private WeakReference<Player> playerRef = null;
	
	@Id @Auto 
	private int id;
	
	@Index("uuidIndex")
	private long uuidMost;
	
	@Index("uuidIndex")
	private long uuidLeast;
	
	@Var @Size(16) 
	private String name;
	
	@Default(Default.UPDATE_TIMESTAMP)
	private Timestamp lastOnline;

	public PlayerID() {}
	
	protected PlayerID(int id) throws SQLException {
		this.id = id;
	}	

	protected PlayerID(Player player) throws SQLException {
		UUID uniqueId = player.getUniqueId();
		uuidLeast = uniqueId.getLeastSignificantBits();
		uuidMost = uniqueId.getMostSignificantBits();
		update(player);
	}
	
	protected PlayerID(String name, UUID uuid) throws SQLException {
		this.name = name;
		uuidLeast = uuid.getLeastSignificantBits();
		uuidMost = uuid.getMostSignificantBits();
	}

	protected void update(Player player) {
		name = player.getName();
		playerRef = new WeakReference<Player>(player); 
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public Timestamp getLastOnline() {
		return lastOnline;
	}
	
	public UUID getUUID() {
		return new UUID(uuidMost, uuidLeast);
	}
	
	/**
	 * @return The Player object if they are online, or null if they are offline.
	 */
	public Player getPlayer() {
		return (playerRef == null ? null : playerRef.get());
	}
	
	@Override
	public boolean equals(Object object) {
		if (object instanceof PlayerID) {
			return ((PlayerID)object).getId() == this.getId();
		}
		return false;
	}
}
