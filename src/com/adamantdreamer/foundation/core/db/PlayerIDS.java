package com.adamantdreamer.foundation.core.db;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import org.bukkit.entity.Player;

import com.adamantdreamer.foundation.core.Foundation;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;


public class PlayerIDS {
	
	static private Map<Player, PlayerID> idMap = new WeakHashMap<>();
	
	static private Query<PlayerID> queryUUID = Foundation.dao().read(PlayerID.class).where("uuidLeast = ? AND uuidMost = ?");
	static private Query<PlayerID> queryName = Foundation.dao().read(PlayerID.class).where("name LIKE ?");
	
	static public PlayerID get(Player player) throws SQLException {
		if (idMap.containsKey(player)) {
			return idMap.get(player);
		}
		
		PlayerID pid;
		UUID uuid = player.getUniqueId();
		QueryResult<PlayerID> select = queryUUID.read(
				uuid.getLeastSignificantBits(), uuid.getMostSignificantBits());
		if (select.isBeforeFirst()) {
			pid = select.first();
			pid.update(player);
			Foundation.dao().update(pid);
		} else {
			pid = new PlayerID(player);
			Foundation.dao().create(pid);
		}
		
		idMap.put(player, pid);
		return pid;
	}
		
	static public PlayerID get(String name, UUID uuid) throws SQLException {
		QueryResult<PlayerID> select = queryUUID.read(
				uuid.getLeastSignificantBits(), uuid.getMostSignificantBits());
		if (select.isBeforeFirst()) {
			return select.first();
		} else {
			PlayerID pid = new PlayerID(name, uuid);
			Foundation.dao().create(pid);
			return pid;
		}
	}	
	
	static public PlayerID get(int id) throws SQLException {
		PlayerID pid = new PlayerID(id);
		return Foundation.dao().read(pid); 
	}

	/**
	 * @param name Player Name. Use % for wildcards.
	 * @return PlayerID, or null if not found.
	 * @throws SQLException
	 */
	static public List<PlayerID> get(String name) throws SQLException {
		return queryName.read(name).readAll();
	}
	
}
