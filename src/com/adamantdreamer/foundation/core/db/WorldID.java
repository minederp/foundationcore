package com.adamantdreamer.foundation.core.db;

import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.DefineIndex;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Var;
import com.adamantdreamer.quorm.index.IndexType;

@DefineIndex(value="uuidIndex", type=IndexType.UNIQUE_KEY)
public class WorldID {

	@Id @Auto
	public int id;

	@Size(32) @Var
	private String server;

	@Size(32) @Var
	private String name;
	
	protected WorldID(int id) {
		this.id = id;
	}
	
	public WorldID(String server, String name) {
		if (server.length() > 32) {
			this.server = server.substring(0,32); 
		} else {
			this.server = server;
		}
		if (name.length() > 32) {
			this.name = name.substring(0,32); 
		} else {
			this.name = server;
		}
		
	}
	
}
