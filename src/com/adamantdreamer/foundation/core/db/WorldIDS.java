package com.adamantdreamer.foundation.core.db;

import java.sql.SQLException;
import java.util.Map;
import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.adamantdreamer.foundation.core.Foundation;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;

public class WorldIDS {

	static private Query<WorldID> queryName = Foundation.dao().read(WorldID.class).where("server LIKE ? AND name LIKE ?");
	static private Map<World, WorldID> worldMap = new WeakHashMap<>();  
	
	static public WorldID get(World world) throws SQLException {

		// If already in map, return
		if (worldMap.containsKey(world)) {
			return worldMap.get(world);
		}

		WorldID result = get(Bukkit.getServerName(), world.getName());
		if (result == null) {
			result = new WorldID(Bukkit.getServerName(), world.getName());
			Foundation.dao().create(result);
		}
		
		worldMap.put(world, result);
		return result;
	}
	
	static public WorldID get(String server, String name) throws SQLException {
		if (server.length() > 32) {
			server = server.substring(0,32);
		}
		if (name.length() > 32) {
			name = name.substring(0,32);
		}
		QueryResult<WorldID> result = queryName.read(server, name);
		if (result.isBeforeFirst()) {
			return result.first();
		} else {
			return null;
		}
	}
	
	static public WorldID get(int id) throws SQLException {
		return Foundation.dao().read(new WorldID(id));
	}
}
