package com.adamantdreamer.foundation.core.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerBlockstepEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Block from, to;
    
	public PlayerBlockstepEvent(Player player, Block from, Block to) {
		this.player = player;
		this.from = from;
		this.to = to;
		// TODO Auto-generated constructor stub
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public Player getPlayer() {
    	return this.player;
    }
    
    public Block getFrom() {
    	return this.from;
    }
    
    public Block getTo() {
    	return this.to;
    }
    
}
