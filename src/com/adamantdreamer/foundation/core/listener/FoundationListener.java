package com.adamantdreamer.foundation.core.listener;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.adamantdreamer.foundation.core.Foundation;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import com.adamantdreamer.foundation.core.event.PlayerBlockstepEvent;

public class FoundationListener implements Listener {

//// PlayerID ////

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void PlayerLogin(PlayerLoginEvent event) {
    	try {
			PlayerIDS.get(event.getPlayer());
		} catch (SQLException ex) {
			Foundation.getPlugin().logError("Error with PlayerIDS", ex);
		}
    	
    	// Blockstep event on login
    	Bukkit.getServer().getPluginManager().callEvent(new PlayerBlockstepEvent(event.getPlayer(), null, event.getPlayer().getLocation().getBlock())); 
	}
	
//// Player Blockstep Event //// 

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void PlayerMoveCheck(PlayerMoveEvent e){
		 Block from = e.getFrom().getBlock();
		 Block to = e.getTo().getBlock();
		 if (from != null && to != null && !(to.equals(from))){
            Bukkit.getServer().getPluginManager().callEvent(new PlayerBlockstepEvent(e.getPlayer(), e.getFrom().getBlock(), e.getTo().getBlock()));
        }
    }

}
